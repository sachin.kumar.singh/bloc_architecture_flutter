import 'package:demo_bloc_app/src/modules/user/view/splash_screen.dart';
import 'package:flutter/material.dart';


class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  const MaterialApp(
      title: 'Essentia Bloc',
      home: SplashScreen(),
    );
  }
}

class Application {
  static BuildContext? _context;

  Application(BuildContext context) {
    _context = context;
  }

  static BuildContext? getContext() {
    return _context;
  }
}