
import 'package:demo_bloc_app/src/utils/constants/Styles.dart';
import 'package:demo_bloc_app/src/utils/constants/color.dart';
import 'package:demo_bloc_app/src/utils/constants/size_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'tap_widget.dart';

class CustomTextField extends StatefulWidget {
  final String? hintText;
  final TextInputType? keyboardType;
  final Function()? iconClick;
  final Function()? onTap;
  final Function(String)? onChange;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final bool obscureText;
  final bool readOnly;
  final bool isFocused;
  final TextEditingController controller;
  final FocusNode? formFocusNode;
  final String? Function(String?)? validations;
  final List<TextInputFormatter>? inputFormatters;
  final bool isTextChanged;
  final double textSize;
  final TextStyle? style;
  final Color fillColor;
  final int maxLines;

  CustomTextField(
      { this.validations,
       this.hintText,
      required this.controller,
      this.onChange,
      this.onTap,
      this.textSize = 20,
      this.isTextChanged = true,
      this.isFocused = false,
      this.keyboardType,
      this.obscureText = false,
      this.iconClick,
      this.suffixIcon,
      this.prefixIcon,
      this.readOnly = false,
      this.maxLines = 1,
      this.fillColor = Colors.white,
      this.formFocusNode,
      this.style,
      this.inputFormatters,  });

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: TextFormField(
          
            controller: widget.controller,
            keyboardType: widget.keyboardType,
            validator: widget.validations,
            textInputAction: widget.maxLines>1?TextInputAction.newline:TextInputAction.done,
            obscureText: widget.obscureText,
            onChanged: widget.onChange,
            readOnly: widget.readOnly,
            expands: false,
            maxLines: widget.maxLines,
            onTap: widget.onTap,
            cursorColor: Colors.black,
            style: widget.style ??
                Styles.light(
                    size: widget.textSize, color: ColorConstants.BLACK_OP70),
            decoration: InputDecoration(
              errorMaxLines: 4,
              hintText: widget.hintText,
              fillColor: widget.isTextChanged
                  ? widget.fillColor
                  : ColorConstants.WHITE,
              filled: true,
              isDense: true,
              border: OutlineInputBorder(
                  borderSide:  BorderSide(
                      color: widget.isTextChanged
                          ? ColorConstants.LIGHT_GREY
                          : ColorConstants.SPLASH_BG,
                      width: SizeConstants.BORDER_WIDTH_1)),
              enabledBorder: OutlineInputBorder(
                  borderSide:  BorderSide(
                      color: widget.isTextChanged
                          ? ColorConstants.LIGHT_GREY
                          : ColorConstants.SPLASH_BG,
                      width: SizeConstants.BORDER_WIDTH_1)),
              errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                      color: Color.fromARGB(255, 223, 6, 6),
                      width: SizeConstants.BORDER_WIDTH_1)),
              disabledBorder:  OutlineInputBorder(
                  borderSide:  BorderSide(
                      color: widget.isTextChanged
                          ?  ColorConstants.LIGHT_GREY
                          : ColorConstants.SPLASH_BG,
                      width: SizeConstants.BORDER_WIDTH_1)),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    color: widget.isTextChanged
                        ?  ColorConstants.LIGHT_GREY
                        : ColorConstants.SPLASH_BG,
                    width: SizeConstants.BORDER_WIDTH_1),
              ),
              suffixIcon: widget.suffixIcon != null
                  ? TapWidget(
                      onTap: widget.iconClick,
                      child: widget.suffixIcon,
                    )
                  : null,
              prefixIcon: widget.prefixIcon,
            )));
  }
}
