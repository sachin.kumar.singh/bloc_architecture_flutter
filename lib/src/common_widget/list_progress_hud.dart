import 'package:flutter/material.dart';
import 'custom_shimmer_widget.dart';

class ListProgressHud extends StatefulWidget {
  final bool? isLoading;
  final Widget? body;

  ListProgressHud({this.isLoading, this.body});

  @override
  _ListProgressHudState createState() => _ListProgressHudState();
}

class _ListProgressHudState extends State<ListProgressHud> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: widget.body,
          ),
          Visibility(
            visible: widget.isLoading!,
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                color: Colors.white,
                child: ListView.builder(
                    itemCount: 5,
                    primary: false,
                    padding: EdgeInsets.only(top: 0),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        leading: CustomShimmerWidget.circular(height: 64, width: 64),
                        title: Align(
                          alignment: Alignment.centerLeft,
                          child: CustomShimmerWidget.rectangular(height: 16,
                            width: MediaQuery.of(context).size.width*0.3,),
                        ),
                        subtitle: CustomShimmerWidget.rectangular(height: 14),
                      );
                    })),
          ),
        ],
      ),
    );
  }
}
