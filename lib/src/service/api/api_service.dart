import 'dart:developer';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'environment.dart';

class ApiService {
  static const _timeout = 5000;
  late Dio dio;

  Environment _env = _Prod();

  Environment get env => _env;

  Dio init() {
    final options = BaseOptions(
        baseUrl: _env.baseUrl,
        connectTimeout: _timeout,
        receiveTimeout: _timeout,
        //TODO: add any headers here if desired
        headers: {});
    dio = Dio(options);
    if (kDebugMode) {
      dio.interceptors.add(LogInterceptor(
        responseBody: true,
        requestHeader: false,
        responseHeader: false,
      ));
    }
    return dio;
  }

  void setEnvironment(EnvironmentType type) {
    log("Setting environment to $type");
    switch (type) {
      case EnvironmentType.dev:
        _env = _Dev();
        break;
      default:
        _env = _Prod();
    }
  }
}

class _Prod extends Environment {
  @override
  EnvironmentType get type => EnvironmentType.prod;

  @override
  String get baseUrl => "https://api.realworld.io/api/";

  //TODO: add your api key here
  @override
  String get apiKey => "For api key";
}

class _Dev extends Environment {
  @override
  EnvironmentType get type => EnvironmentType.dev;

  @override
  String get baseUrl => "https://api.realworld.io/api/";

  //TODO: add your api key here
  @override
  String get apiKey => "For api key";
}
