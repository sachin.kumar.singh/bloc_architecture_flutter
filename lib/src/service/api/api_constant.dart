class ApiConstant{
  static const article = "articles";
  static const signup = "users";
  static const signin = "users/login";
}