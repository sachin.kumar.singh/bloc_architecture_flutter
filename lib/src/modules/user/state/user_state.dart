


import 'package:demo_bloc_app/src/modules/user/model/response/articles_response.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/login_response.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/register_response.dart';

abstract class UserState {}

class UserInitial extends UserState {}



class ArticlesLoading extends UserState {}

class ArticlesSuccess extends UserState {
  final ArticlesResponse response;
  ArticlesSuccess(this.response);
}
class ArticlesError extends UserState {
  final String error;
  ArticlesError(this.error);
}

/////
class SignUpLoading extends UserState {}

class SignUpSuccess extends UserState {
  final RegisterResponse response;
  SignUpSuccess(this.response);
}
class SignUpError extends UserState {
  final String error;
  SignUpError(this.error);
}

///
class SignInLoading extends UserState {}

class SignInSuccess extends UserState {
    final LoginResponse response;
    SignInSuccess(this.response);
}
class SignInError extends UserState {
  final String error;
  SignInError(this.error);
}


