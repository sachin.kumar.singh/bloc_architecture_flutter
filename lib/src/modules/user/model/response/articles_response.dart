class ArticlesResponse {
  List<Articles>? articles;
  int? articlesCount;

  ArticlesResponse({this.articles, this.articlesCount});

  ArticlesResponse.fromJson(Map<String, dynamic> json) {
    if (json['articles'] != null) {
      articles = <Articles>[];
      json['articles'].forEach((v) {
        articles!.add(new Articles.fromJson(v));
      });
    }
    articlesCount = json['articlesCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.articles != null) {
      data['articles'] = this.articles!.map((v) => v.toJson()).toList();
    }
    data['articlesCount'] = this.articlesCount;
    return data;
  }
}

class Articles {
  String? slug;
  String? title;
  String? description;
  String? body;
  List<String>? tagList;
  String? createdAt;
  String? updatedAt;
  bool? favorited;
  int? favoritesCount;
  Author? author;

  Articles(
      {this.slug,
      this.title,
      this.description,
      this.body,
      this.tagList,
      this.createdAt,
      this.updatedAt,
      this.favorited,
      this.favoritesCount,
      this.author});

  Articles.fromJson(Map<String, dynamic> json) {
    slug = json['slug'];
    title = json['title'];
    description = json['description'];
    body = json['body'];
    tagList = json['tagList'].cast<String>();
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    favorited = json['favorited'];
    favoritesCount = json['favoritesCount'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slug'] = this.slug;
    data['title'] = this.title;
    data['description'] = this.description;
    data['body'] = this.body;
    data['tagList'] = this.tagList;
    data['createdAt'] = this.createdAt;
    data['updatedAt'] = this.updatedAt;
    data['favorited'] = this.favorited;
    data['favoritesCount'] = this.favoritesCount;
    if (this.author != null) {
      data['author'] = this.author!.toJson();
    }
    return data;
  }
}

class Author {
  String? username;
  Null? bio;
  String? image;
  bool? following;

  Author({this.username, this.bio, this.image, this.following});

  Author.fromJson(Map<String, dynamic> json) {
    username = json['username'];
    bio = json['bio'];
    image = json['image'];
    following = json['following'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['bio'] = this.bio;
    data['image'] = this.image;
    data['following'] = this.following;
    return data;
  }
}

/*class TagList {
  String? text1;
  String? text2;
  String? text3;
  String? text4;
  String? text5;

  

  TagList({this.text1, this.text2, this.text3, this.text4, this.text5});

  TagList.fromJson(Map<String, dynamic> json) {
    text1 = json['text1'];
    text2 = json['text2'];
    text3 = json['text3'];
    text4 = json['text4'];
    text5 = json['text5'];
    
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['text1'] = this.text1;
    data['text2'] = this.text2;
    data['text1'] = this.text3;
    data['text1'] = this.text4;
    return data;
  }
}*/
/*class TagList {
  List<String>? tagList;

  TagList({this.tagList});

  TagList.fromJson(Map<String, dynamic> json) {
    tagList = json['tagList'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['tagList'] = this.tagList;
    return data;
  }
}*/
