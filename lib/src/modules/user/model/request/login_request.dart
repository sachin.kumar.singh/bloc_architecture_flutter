class LoginRequest {
  LoginUser? user;

  LoginRequest({this.user});

  LoginRequest.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new LoginUser.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class LoginUser {
  String? email;
  String? password;

  LoginUser({this.email, this.password});

  LoginUser.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    return data;
  }
}