class RegisterRequest {
  RegisterUser? user;

  RegisterRequest({this.user});

  RegisterRequest.fromJson(Map<String, dynamic> json) {
    user = json['user'] != null ? new RegisterUser.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class RegisterUser {
  String? email;
  String? password;
  String? username;

  RegisterUser({this.email, this.password, this.username});

  RegisterUser.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    password = json['password'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['password'] = this.password;
    data['username'] = this.username;
    return data;
  }
}