import 'package:demo_bloc_app/src/common_widget/custom_text_field.dart';
import 'package:demo_bloc_app/src/common_widget/next_page_route.dart';
import 'package:demo_bloc_app/src/common_widget/progress_hud.dart';
import 'package:demo_bloc_app/src/modules/user/cubit/user_cubit.dart';
import 'package:demo_bloc_app/src/modules/user/model/request/register_request.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/register_response.dart';
import 'package:demo_bloc_app/src/modules/user/view/login_page.dart';
import 'package:demo_bloc_app/src/utils/constants/size_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/log.dart';
import '../../../utils/utility.dart';
import '../state/user_state.dart';

enum LoaderState { initial, loading, success, error }

class RegisterScreenPage extends StatefulWidget {
  const RegisterScreenPage({Key? key}) : super(key: key);

  @override
  RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreenPage> {
  late double _height;
  late double _width;
  User? userDataesList;
  bool isLoading = false;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _userNameController = TextEditingController();
  LoaderState loader = LoaderState.initial;
  
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return BlocProvider(
      create: (context) => UserCubit(),
      child: BlocConsumer<UserCubit, UserState>(listener: (context, state) {
        _handleUserListResponse(state, context);
      }, builder: (context, state) {
        if (state is UserInitial) {
          Log.v("UserInitial");
          UserCubit.get(context).userList();
        }

        if (state is SignUpLoading) {
          Log.v("SignUpLoading");
          isLoading = true;
        }
        if (state is SignUpSuccess) {
          Log.v("SignUpSuccess");
          isLoading = false;
        }
        if (state is SignUpError) {
          Log.v("SignUpError");
          isLoading = false;
        }
        return Scaffold(
          body: SafeArea(
            child: ProgressHud(
              isLoading: isLoading,
              child: Container(
                height: double.infinity,
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: ScrollConfiguration(
                  behavior: const ScrollBehavior(),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        sizedHeight(height: SizeConstants.HEIGHT_50),
                        const Text(
                          "Sign Up",
                          style: TextStyle(
                              fontSize: 40,
                              fontWeight: FontWeight.normal,
                              color: Colors.black87),
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        const Text(
                          "Have an account?",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 14, 223, 122)),
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        CustomTextField(
                          hintText: "Username",
                          controller: _userNameController,
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        CustomTextField(
                          hintText: "Email",
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        CustomTextField(
                          hintText: "Password",
                          controller: _passwordController,
                          
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            isLoading == true
                                ? Container(
                                    decoration: BoxDecoration(
                                      color: Colors.black38,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    alignment: Alignment.center,
                                    //color: Colors.greenAccent,
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.07,
                                    child: Text("Sing Up"),
                                  )
                                : InkWell(
                                    onTap: () {
                                      UserCubit.get(context).userRegister(
                                          request: RegisterRequest(
                                              user: RegisterUser(
                                                  email: _emailController.text,
                                                  password:
                                                      _passwordController.text,
                                                  username: _userNameController
                                                      .text)));
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      alignment: Alignment.center,
                                      //color: Colors.black54,
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.07,
                                      child: Text("Sing Up"),
                                    ),
                                  ),
                          ],
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_15),
                        InkWell(
                          onTap: () {
                            Navigator.push(context, NextPageRoute(LoginPage()));
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Already have account?",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Color.fromARGB(255, 14, 223, 122)),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  void _handleUserListResponse(UserState state, BuildContext context) {
    if (state is SignUpSuccess) {
      userDataesList = state.response.user;
      Utility.showSnackBar(
          message: "Registration Successful", context: context);
      Navigator.push(context, NextPageRoute(LoginPage()));
    }
    if (state is SignUpError) {
      Utility.showSnackBar(message: state.error, context: context);
    }
  }

  
}
