
import 'package:demo_bloc_app/src/common_widget/next_page_route.dart';
import 'package:demo_bloc_app/src/modules/user/view/login_page.dart';
import 'package:demo_bloc_app/src/utils/utility.dart';
import 'package:flutter/material.dart';


class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  
    waitFor(3).then((value) {
      Navigator.pushAndRemoveUntil(context, NextPageRoute(LoginPage()), (route) => false);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: screenHeight(context),
        width: screenWidth(context),
        color: Colors.greenAccent,
        child: Center(
          child: Card(child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text("Welcome", style: TextStyle(fontSize: 25),),
          )),
        ),
      ),
    );
  }
}
