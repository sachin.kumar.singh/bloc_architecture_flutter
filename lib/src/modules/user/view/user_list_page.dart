
import 'package:demo_bloc_app/src/common_widget/progress_hud.dart';
import 'package:demo_bloc_app/src/modules/user/cubit/user_cubit.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/articles_response.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/log.dart';
import '../../../utils/utility.dart';
import '../state/user_state.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({Key? key}) : super(key: key);

  @override
  UserListState createState() => UserListState();
}

class UserListState extends State<UserListPage> {
  List<Articles>? articlesList = [];
  List<String> tagListResponse = [];
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserCubit(),
      child: BlocConsumer<UserCubit, UserState>(listener: (context, state) {
        _handleUserListResponse(state, context);
      }, builder: (context, state) {
        if (state is UserInitial) {
          Log.v("UserInitial");
          UserCubit.get(context).userList();
        }

        if (state is ArticlesLoading) {
          Log.v("UserListLoading");
          isLoading = true;
        }
        if (state is ArticlesSuccess) {
          Log.v("UserListSuccess");
          isLoading = false;
        }
        if (state is ArticlesError) {
          Log.v("UserListError");
          isLoading = false;
        }
        return Scaffold(
          appBar: _appBarBuilder(context),
          body: SafeArea(
            child: ProgressHud(
              isLoading: isLoading,
              child: Container(
                height: double.infinity,
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: ScrollConfiguration(
                  behavior: const ScrollBehavior(),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        articlesListVieewBuilder(context),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  void _handleUserListResponse(UserState state, BuildContext context) {
    if (state is ArticlesSuccess) {
      articlesList = state.response.articles;
      Log.v("_handleUserListResponse success");
    }
    if (state is ArticlesError) {
      Utility.showSnackBar(message: state.error, context: context);
    }
  }

  articlesListVieewBuilder(BuildContext context) {
    return articlesList!.isNotEmpty
        ? ListView.builder(
            scrollDirection: Axis.vertical,
            primary: false,
            shrinkWrap: true,
            padding: const EdgeInsets.only(top: 25),
            itemCount: articlesList!.length,
            itemBuilder: (context, index) {
              return _articlesListItem(context, index);
            },
          )
        : Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: const Center(child: Text("No Data found")));
  }

  _articlesListItem(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 10),
      child: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.all(1),
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 2,
              offset: Offset(0, 1),
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      SizedBox(
                        height: 50,
                        width: 50,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(50.0),
                            child: Image.network(
                                fit: BoxFit.fill,
                                articlesList?[index].author?.image ?? "",
                                errorBuilder: (context, _, __) {
                              return const Icon(Icons.error_outline);
                            }),
                          ),
                        ),
                      ),
                      sizedWidth(),
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: Text(
                                  articlesList?[index].author?.username ?? "",
                                  style:
                                      Theme.of(context).textTheme.labelMedium,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                              sizedWidth(width: 80),
                              Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: 1.0,color: Color.fromARGB(255, 5, 233, 123)),
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(5.0) //
                                      ),
                                ),
                                height: 25,
                                width: 60,
                                child:
                                    Center(child: Icon(Icons.favorite_border,color: Colors.black87,)),
                              )
                            ],
                          ),
                          Container(
                            alignment: Alignment.bottomLeft,
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(4.0),
                                  child: Text(
                                    Utility.convertSelectedCalendarDate(
                                        DateTime.parse(articlesList?[index]
                                                .createdAt
                                                .toString() ??
                                            "")),
                                    style:
                                        Theme.of(context).textTheme.labelMedium,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 70),
                                  child: Text(
                                    articlesList?[index]
                                            .favoritesCount
                                            .toString() ??
                                        "",
                                    style:
                                        Theme.of(context).textTheme.labelMedium,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  sizedHeight(),
                ],
              ),
              Text(articlesList?[index].title ?? ""),
              sizedHeight(),
              Text(
                articlesList?[index].description ?? "",
                style: Theme.of(context).textTheme.labelMedium,
                overflow: TextOverflow.ellipsis,
              ),
              sizedHeight(),
              _tagListBuilder(context, index),
            ],
          ),
        ),
      ),
    );
  }

  _appBarBuilder(BuildContext context) {
    return PreferredSize(
        preferredSize: Size.fromHeight(150.0),
        child: AppBar(
          backgroundColor: Colors.green,
          centerTitle: true,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(40),
            ),
          ),
          flexibleSpace: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Container(
                height: 60,
                width: MediaQuery.of(context).size.width * 0.8,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Icon(
                      Icons.book,
                      color: Colors.black,
                    ),
                    Text(
                      "All Articles",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.black),
                    ),
                    sizedWidth(width: 30),
                    Icon(
                      Icons.article,
                      color: Colors.white70,
                    ),
                    Text(
                      "My Articles",
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.white70),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 50),
                child: Row(
                  children: [
                    Container(
                      color: Colors.black,
                      height: 2,
                      width: MediaQuery.of(context).size.width * 0.3,
                    ),
                  ],
                ),
              )
            ],
          ),
          actions: [
            SizedBox(
              height: 500,
              width: MediaQuery.of(context).size.width * 0.7,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Home",
                    style: TextStyle(
                        fontSize: 20,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                  const SizedBox(
                    width: 60,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(50.0),
                      child: Image.network(
                          fit: BoxFit.fill,
                          "https://api.realworld.io/images/smiley-cyrus.jpeg",
                          errorBuilder: (context, _, __) {
                        return const Icon(Icons.error_outline);
                      }),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ));
  }

  _tagListBuilder(BuildContext context, int index) {
    return Container(
      height: 40,
      width: screenWidth(context),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        primary: false,
        shrinkWrap: true,
        itemCount: articlesList?[index].tagList?.length,
        itemBuilder: (context, tagIndex) {
          return _tagListItem(context, tagIndex, index);
        },
      ),
    );
  }

  _tagListItem(BuildContext context, int tagIndex, int index) {
    return Padding(
      padding: const EdgeInsets.only(left: 15),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
              decoration: BoxDecoration(
                border: Border.all(width: 1.0, color: Color.fromARGB(255, 5, 226, 119)),
                borderRadius: BorderRadius.all(Radius.circular(5.0)),
              ),
              height: 35,
              //width: 60,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                  child: Text(articlesList?[index].tagList?[tagIndex] ?? "", style: TextStyle(color: Color.fromARGB(255, 3, 248, 130)),),
                ),
              ))
        ],
      ),
    );
  }
}
