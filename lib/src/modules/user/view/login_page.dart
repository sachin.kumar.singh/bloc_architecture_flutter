import 'package:demo_bloc_app/src/common_widget/custom_text_field.dart';
import 'package:demo_bloc_app/src/common_widget/next_page_route.dart';
import 'package:demo_bloc_app/src/common_widget/progress_hud.dart';
import 'package:demo_bloc_app/src/modules/user/cubit/user_cubit.dart';
import 'package:demo_bloc_app/src/modules/user/model/request/login_request.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/register_response.dart';
import 'package:demo_bloc_app/src/modules/user/view/register_screen_page.dart';
import 'package:demo_bloc_app/src/modules/user/view/user_list_page.dart';
import 'package:demo_bloc_app/src/utils/constants/size_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../../utils/log.dart';
import '../../../utils/utility.dart';
import '../state/user_state.dart';

enum LoaderState { initial, loading, success, error }

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  late double _height;
  late double _width;
  User? userDataesList;
  bool isLoading = false;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  LoaderState loader = LoaderState.initial;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _height = MediaQuery.of(context).size.height;
    _width = MediaQuery.of(context).size.width;
    return BlocProvider(
      create: (context) => UserCubit(),
      child: BlocConsumer<UserCubit, UserState>(listener: (context, state) {
        _handleUserListResponse(state, context);
      }, builder: (context, state) {
        if (state is UserInitial) {
          Log.v("UserInitial");
          UserCubit.get(context).userList();
        }

        if (state is SignInLoading) {
          Log.v("SignLoading");
          isLoading = true;
        }
        if (state is SignInSuccess) {
          Log.v("SignSuccess");
          isLoading = false;
        }
        if (state is SignInError) {
          Log.v("SignError");
          isLoading = false;
        }
        return Scaffold(
          body: SafeArea(
            child: ProgressHud(
              isLoading: isLoading,
              child: Container(
                height: double.infinity,
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 25),
                child: ScrollConfiguration(
                  behavior: const ScrollBehavior(),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        sizedHeight(height: SizeConstants.HEIGHT_40),
                        const Text(
                          "Sign In",
                          style: TextStyle(
                              fontSize: 50,
                              fontWeight: FontWeight.normal,
                              color: Colors.black87),
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_40),
                        InkWell(
                            onTap: () {
                              Navigator.push(
                                  context, NextPageRoute(RegisterScreenPage()));
                            },
                            child: const Text(
                              "Need an account?",
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal,
                                  color: Color.fromARGB(255, 14, 223, 122)),
                            )),
                        sizedHeight(height: SizeConstants.HEIGHT_40),
                        CustomTextField(
                          hintText: "Email",
                          controller: _emailController,
                          keyboardType: TextInputType.emailAddress,
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        CustomTextField(
                          hintText: "Password",
                          controller: _passwordController,
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_20),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            isLoading == true
                                ? Container(
                                    decoration: BoxDecoration(
                                      color: Colors.black38,
                                      borderRadius: BorderRadius.circular(5),
                                    ),
                                    alignment: Alignment.center,
                                    //color: Colors.greenAccent,
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    height: MediaQuery.of(context).size.height *
                                        0.07,
                                    child: Text("Sing In"),
                                  )
                                : InkWell(
                                    onTap: () {
                                      UserCubit.get(context).userLogin(
                                          request: LoginRequest(
                                              user: LoginUser(
                                                  email: _emailController.text,
                                                  password: _passwordController
                                                      .text)));
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                        color: Colors.green,
                                        borderRadius: BorderRadius.circular(5),
                                      ),
                                      alignment: Alignment.center,
                                      //color: Colors.black54,
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.07,
                                      child: Text("Sign In"),
                                    ),
                                  ),
                          ],
                        ),
                        sizedHeight(height: SizeConstants.HEIGHT_15),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      }),
    );
  }

  void _handleUserListResponse(UserState state, BuildContext context) {
    if (state is SignInSuccess) {
      Navigator.pushAndRemoveUntil(
          context, NextPageRoute(UserListPage()), (route) => false);
      Log.v("SignUp success");
    }
    if (state is SignInError) {
      Utility.showSnackBar(message: state.error, context: context);
    }
  }
}
