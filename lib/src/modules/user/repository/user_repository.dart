import 'dart:convert';
import 'dart:io';
import 'package:demo_bloc_app/src/modules/user/model/request/login_request.dart';
import 'package:demo_bloc_app/src/modules/user/model/request/register_request.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/articles_response.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/login_response.dart';
import 'package:demo_bloc_app/src/modules/user/model/response/register_response.dart';
import 'package:demo_bloc_app/src/service/api/api_service.dart';
import 'package:dio/dio.dart';
import '../../../service/api/api_constant.dart';
import '../../../service/api/api_exception.dart';
import '../../../utils/log.dart';


class UserRepository {
  Future<ArticlesResponse> getEmployeeList() async {
    try {
      final api = ApiService().init();
      final apiResponse = await api.get(ApiConstant.article);
      if (apiResponse.statusCode! >= 200 && apiResponse.statusCode! <= 300) {
        final result =
            json.decode((apiResponse.toString())) as Map<String, dynamic>;
        final response = ArticlesResponse.fromJson(result);

        return response;
      } else {
        throw ApiException(
            message:
                apiResponse.statusMessage ?? "Something went wrong. Try again.",
            statusCode: apiResponse.statusCode!);
      }
    } on SocketException catch (exception) {
      Log.v("Repository exception $exception");
      throw ApiException(
          message: "Please check your internet connection or try again later.");
    } on DioError catch (exception) {
      Log.v("Repository exception$exception");
      throw ApiException(message: exception.response?.data['message'].toString() ??
              "Something Went Wrong"
);
    }
  }

  Future<RegisterResponse> userRegisterData(
      {required RegisterRequest request}) async {
    try {
      final api = ApiService().init();
      final apiResponse =
          await api.post(ApiConstant.signup, data: request.toJson());
      if (apiResponse.statusCode! >= 200 && apiResponse.statusCode! <= 300) {
        final result =
            json.decode((apiResponse.toString())) as Map<String, dynamic>;
        final response = RegisterResponse.fromJson(result);
        return response;
      } else {
        throw ApiException(
            message:
                apiResponse.statusMessage ?? "Something went wrong. Try again.",
            statusCode: apiResponse.statusCode!);
      }
    } on SocketException catch (exception) {
      Log.v("Repository exception$exception");
      throw ApiException(
          message: "Please check your internet connection or try again later.");
    } on DioError catch (exception) {
      Log.v("Repository exception $exception");
      throw ApiException(message: exception.response?.data['message'].toString() ??
              "Something Went Wrong");
    }
  }

  Future<LoginResponse> userLoginData({required LoginRequest request}) async {
    try {
      final api = ApiService().init();
      final apiResponse =
          await api.post(ApiConstant.signin, data: request.toJson());
      if (apiResponse.statusCode! >= 200 && apiResponse.statusCode! <= 300) {
        final result =
            json.decode((apiResponse.toString())) as Map<String, dynamic>;
        final response = LoginResponse.fromJson(result);
        return response;
      } else {
        throw ApiException(
            message:
                apiResponse.statusMessage ?? "Something went wrong. Try again.",
            statusCode: apiResponse.statusCode!);
      }
    } on SocketException catch (exception) {
      Log.v("Repository exception $exception");
      throw ApiException(
          message: "Please check your internet connection or try again later.");
    } on DioError catch (exception) {
      Log.v("Repository exception $exception");
      throw ApiException(message: exception.response?.data['message'].toString() ??
              "Something Went Wrong");
    }
  }
}
