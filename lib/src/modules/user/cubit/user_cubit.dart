
import 'package:demo_bloc_app/src/modules/user/model/request/login_request.dart';
import 'package:demo_bloc_app/src/modules/user/model/request/register_request.dart';
import 'package:demo_bloc_app/src/modules/user/repository/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../state/user_state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super( UserInitial());

  static UserCubit get(context) => BlocProvider.of(context);
  final _userRepository = UserRepository();

  Future<void> userList() async {
    try {
      emit( ArticlesLoading());
      final response = await _userRepository.getEmployeeList();
      if (response.articles!.isNotEmpty) {
        emit(ArticlesSuccess(response));
      } else {
        emit( ArticlesError("No data found"));
      }
    } catch (e) {
      emit( ArticlesError("Something went wrong"));
    }
  }

  Future<void> userRegister({required RegisterRequest request}) async {
    try {
      emit( SignUpLoading());
      final response = await _userRepository.userRegisterData(request: request);
      if (response.user != null) {
        emit(SignUpSuccess(response));
      } else {
        emit( SignUpError("No data found"));
      }
    } catch (e) {
      emit( SignUpError("Something went wrong"));
    }
  }

  Future<void> userLogin({required LoginRequest request}) async {
    try {
      emit( SignInLoading());
      final response = await _userRepository.userLoginData(request: request);
      if (response.user != null) {
        emit(SignInSuccess(response));
      } else {
        emit( SignInError("No data found"));
      }
    } catch (e) {
      emit( SignInError( "Something went wrong"));
    }
  }
}
