import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';

import 'log.dart';
double screenHeight(BuildContext context) {
  return MediaQuery.of(context).size.height;
}

double screenWidth(BuildContext context) {
  return MediaQuery.of(context).size.width;
}

SizedBox sizedHeight({double height = 10}) {
  return SizedBox(height: height);
}

SizedBox sizedWidth({double width = 10}) {
  return SizedBox(width: width);
}
Future waitFor(int seconds) async {
  await Future.delayed(Duration(seconds: seconds));
}


void showSnackBar(
    {required String message,
    required BuildContext context,
    int miliSec = 1500}) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      duration: Duration(milliseconds: miliSec),
      content: Text(
        message,
        style: const TextStyle(color: Colors.white),
      ),
      backgroundColor: Colors.black));
}
class Utility {
  static Future<bool> checkNetwork() async {
    bool isConnected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        Log.v('connected');
        isConnected = true;
      }
    } on SocketException catch (_) {
      Log.v('not connected');

      isConnected = false;
    }
    return isConnected;
  }

  static void hideKeyboard() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');

  }

static String convertSelectedCalendarDate(DateTime date) {
    var formatter = new DateFormat('dd/MM/yyyy hh:mm a');
    String formatted = formatter.format(date);
    return formatted;
  }

  static void showSnackBar(
      {required String message,
        required BuildContext context,
        int miliSec = 1500}) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        duration: Duration(milliseconds: miliSec),
        content: Text(
          message,
          style: const TextStyle(color: Colors.white),
        ),
        backgroundColor: Colors.black));
  }

  static String convertDateFormat(DateTime date) {
    var formatter = new DateFormat('dd/MM/yyyy');
    String formatted = formatter.format(date);
    return formatted;
  }
  static Future waitFor(int seconds) async {
    await Future.delayed(Duration(seconds: seconds));
  }

  
}
