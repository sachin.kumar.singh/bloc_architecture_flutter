import 'package:flutter/material.dart';

import 'color.dart';
import 'size_constants.dart';

class Styles {
  static const _PoppinsSemiBold = "Poppins_SemiBold";
  static const _PoppinsLight = "Poppins_Light";
  static const _PoppinsMedium = "Poppins_Medium";
  static const _PoppinsRegular = "Poppins_Regular";

  static semiBold({double size = SizeConstants.TEXT_SIZE_14, Color color = ColorConstants.BLACK}) {
    return TextStyle(
        fontSize: size,
        fontFamily: _PoppinsSemiBold,
        color: color,
        fontWeight: FontWeight.bold);
  }

  static light({double size = SizeConstants.TEXT_SIZE_12, Color color = ColorConstants.BLACK}) {
    return TextStyle(
        fontSize: size,
        fontFamily: _PoppinsLight,
        color: color,
        fontWeight: FontWeight.w400);
  }

  static medium({double size = SizeConstants.TEXT_SIZE_14, Color color = ColorConstants.BLACK}) {
    return TextStyle(
        fontSize: size,
        fontFamily: _PoppinsMedium,
        color: color,
        fontWeight: FontWeight.w600);
  }

  static regular({double size = SizeConstants.TEXT_SIZE_12, Color color = ColorConstants.BLACK}) {
    return TextStyle(
      fontSize: size,
      fontFamily: _PoppinsRegular,
      color: color
    );
  }
}
