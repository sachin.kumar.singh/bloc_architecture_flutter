import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorConstants {
  static const BG_COLOR = Color(0xffFAFAFA);
  static const SPLASH_BG = Color(0xff4E5C65);
  static const WHITE = Color(0xffffffff);
  static const BLACK = Color(0xff000000);
  static const ERROR = Color(0xffFF5E5E);
  static const RED_HEART = Color(0xffFB6161);
  static const ARCHIVE = Color(0xff6CC567);
  static const DARK_GREY = Color(0xff4E5C65);
  static const LIGHT_RED = Color(0xffFFE3D5);
  static const LIGHT_GREY = Color(0xffD8E0E6);
  static const BUTTON_LIGHT_GREY = Color(0xffCBCBCB);
  static const DIVIDER = Color(0xff000001);
  static const SELECTED_COLOR = Color(0xffECF7FF);
  static const BLUE = Color(0xff2E7EF6);
  static var INACTIVE_MENU = Color(0xffFFFFFF).withOpacity(0.3);
  static var BLACK_OP05 = Color(0xff000000).withOpacity(0.05);
  static var BLACK_OP40 = Color(0xff000000).withOpacity(0.4);
  static var BLACK_OP45 = Color(0xff000000).withOpacity(0.45);
  static var BLACK_OP50 = Color(0xff000000).withOpacity(0.5);
  static var BLACK_OP60 = Color(0xff000000).withOpacity(0.6);
  static var BLACK_OP70 = Color(0xff000000).withOpacity(0.7);
  static var BLACK_OP80 = Color(0xff000000).withOpacity(0.8);
  static var BLACK_OP90 = Color(0xff000000).withOpacity(0.9);
  List<Color> hexColor = [
    BLACK,
    ERROR,
    ARCHIVE,
    DARK_GREY,
    LIGHT_RED,
    BLUE
  ];
  List<String> colorString = ["6CC567", "2E7EF6", "FB6161", "4E5C65"];

  static final _random = Random();

  static const SPLASH_BLUE = Color(0xff1E6AC6);
  static var SPLASH_BLUE_OP40 = Color(0xff1E6AC6).withOpacity(0.4);

  static const BUTTON_GREY = Color(0xff707070);

  static const LIGHT_GREEN = Color(0xff059C4B);
  static const PURPLE = Color(0xffD64AFF);
  static const LIGHT_BLUE = Color(0xffEEF5FF);

  static const TRANSPARENT = Colors.transparent;

  String labelColor() {
    return colorString[_random.nextInt(4)];
  }
}
