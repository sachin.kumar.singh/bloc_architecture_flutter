class SizeConstants {
  //page limits

  static const int PAGE_LIMIT_10 = 10;
  static const int PAGE_LIMIT_25 = 25;
  static const int PAGE_LIMIT_50 = 50;
  static const int PAGE_LIMIT_100 = 100;
  static const int PAGE_LIMIT_10000 = 10000;

  // heights

  static const double HEIGHT_10 = 10;
  static const double HEIGHT_12 = 12;
  static const double HEIGHT_15 = 15;
  static const double HEIGHT_20 = 20;
  static const double HEIGHT_25 =25;
  static const double HEIGHT_35 = 35;
  static const double HEIGHT_40 = 40;
  static const double HEIGHT_50 = 50;
  static const double HEIGHT_60 = 60;
  static const double HEIGHT_75 = 75;
  static const double HEIGHT_105 = 105;

  //width

  static const double WIDTH_12 = 12;
  static const double WIDTH_35 = 35;
  static const double WIDTH_40 = 40;
  static const double WIDTH_50 = 50;
  static const double WIDTH_100 = 100;

  //border

  static const double BORDER_WIDTH_1 = 1;
  static const double BORDER_WIDTH_2 = 2;
  static const double BORDER_RADIUS_1 = 1;
  static const double BORDER_RADIUS_5 = 5;
  static const double BORDER_RADIUS_10 = 10;
  static const double BORDER_RADIUS_30 = 30;
  static const double BORDER_RADIUS_50 = 50;

  //paddings

  static const double PADDING_1 = 1;
  static const double PADDING_2 = 2;
  static const double PADDING_4 = 4;
  static const double PADDING_5 = 5;
  static const double PADDING_8 = 8;
  static const double PADDING_10 = 10;
  static const double PADDING_25 = 25;
  static const double PADDING_40 = 40;

  //margin

  static const double MARGIN_10 = 10;
  static const double MARGIN_30 = 30;

  //elevation

  static const double ELEVATION_1 = 1;

  //offset

  static const double OFFSET_MINUS_10 = -10;
  static const double OFFSET_5 = 5;

  //text size

  static const double TEXT_SIZE_10 = 10;
  static const double TEXT_SIZE_11 = 11;
  static const double TEXT_SIZE_12 = 12;
  static const double TEXT_SIZE_14 = 14;
  static const double TEXT_SIZE_16 = 16;
  static const double TEXT_SIZE_18 = 18;
  static const double TEXT_SIZE_20 = 20;
  static const double TEXT_SIZE_28 = 28;
  static const double TEXT_SIZE_30 = 30;























}
