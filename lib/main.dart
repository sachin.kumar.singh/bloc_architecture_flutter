import 'package:demo_bloc_app/src/service/bloc/app_bloc_observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'app.dart';
import 'src/service/api/api_service.dart';

void main()async{
  Bloc.observer =  AppBlocObserver();
  runApp(const MyApp());
  
}